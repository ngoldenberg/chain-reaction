import svg1 from "./assets/1.svg";
import svg2 from "./assets/2.svg";
import svg3 from "./assets/3.svg";
import { Cell, clickCell, Game, Player, undo } from "./logic";

const svg = [svg1, svg2, svg3];
const options = document.getElementById("options") as HTMLDivElement;
const container = document.getElementById("game") as HTMLDivElement;

function renderGame(g: Game) {
  container.innerHTML = "";
  container.style.setProperty("--player", "");
  document.body.style.setProperty("--w", g.w.toString());
  document.body.style.setProperty("--h", g.h.toString());
  const fragment = document.createDocumentFragment();
  for (let y = 0; y < g.h; y++) {
    for (let x = 0; x < g.w; x++) {
      const cell = document.createElement("div");
      cell.addEventListener("click", () => clickCell(g, x, y));
      container.appendChild(cell);
    }
  }
  container.appendChild(fragment);
  document.getElementById("undo")!.addEventListener("click", () => undo(g));
}

function explosion(f: DocumentFragment, dir: string): Promise<void> {
  return new Promise(resolve => {
    const el = document.createElement("div");
    el.innerHTML = svg1;
    el.className = dir;
    el.addEventListener("animationend", () => {
      el.remove();
      resolve();
    });
    f.appendChild(el);
  });
}

function renderExplosion(g: Game, x: number, y: number, p: Player) {
  const cell = container.children[y * g.w + x] as HTMLDivElement;
  cell.style.setProperty("--c", p.color);
  const fragment = document.createDocumentFragment();
  const anims = [];
  if (x !== 0) anims.push(explosion(fragment, "left"));
  if (x !== g.w - 1) anims.push(explosion(fragment, "right"));
  if (y !== 0) anims.push(explosion(fragment, "up"));
  if (y !== g.h - 1) anims.push(explosion(fragment, "down"));
  cell.appendChild(fragment);
  return Promise.all(anims);
}

function renderCell(g: Game, x: number, y: number, c: Cell) {
  const cell = container.children[y * g.w + x] as HTMLDivElement;
  if (c.player) cell.style.setProperty("--c", c.player.color);
  if (c.count === 0) {
    cell.innerHTML = "";
    cell.className = "";
  } else {
    cell.innerHTML = svg[c.count - 1];
    if (c.count === c.threshold - 1) cell.className = "shake";
  }
}

function renderGameOver() {
  alert("Game over!");
  options.className = "open";
}

function renderNextTurn(g: Game) {
  container.style.setProperty("--player", g.players[g.playerI].color);
}

export {
  renderGame,
  renderExplosion,
  renderCell,
  renderGameOver,
  renderNextTurn
};
