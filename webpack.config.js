const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const WebpackPwaManifest = require("webpack-pwa-manifest");
const CleanWebpackPlugin = require("clean-webpack-plugin");
const OfflinePlugin = require("offline-plugin");

module.exports = env => {
  const config = {
    devtool: env.prod ? "source-map" : "cheap-module-inline-source-map",
    entry: "./src/index.ts",
    output: {
      path: path.resolve(__dirname, "public"),
      filename: "script.[contenthash].bundle.js"
    },
    module: {
      rules: [
        {
          test: /\.html$/,
          loader: "html-loader",
          options: {
            attrs: ["img:src", "link:href"]
          }
        },
        {
          test: /\.ts$/,
          use: "ts-loader"
        },
        {
          test: /\.scss$/,
          use: [
            MiniCssExtractPlugin.loader,
            { loader: "css-loader", options: { sourceMap: true } },
            {
              loader: "sass-loader",
              options: {
                sourceMap: true,
                outputStyle: env.dev ? "nested" : "compressed"
              }
            }
          ]
        },
        {
          test: /[123]\.svg$/,
          use: "raw-loader"
        },
        {
          test: /\.(svg|ico|png)$/,
          exclude: /[123]\.svg$/,
          loader: "url-loader",
          options: {
            limit: 8192
          }
        }
      ]
    },
    resolve: {
      extensions: [".js", ".ts"]
    },
    plugins: [
      new CleanWebpackPlugin(["public"]),
      new MiniCssExtractPlugin({
        filename: "style.[contenthash].bundle.css"
      }),
      new HtmlWebpackPlugin({
        template: "src/index.html"
      }),
      new WebpackPwaManifest({
        background_color: "black",
        description: "An engaging strategy game that is way better than chess",
        dir: "ltr",
        display: "fullscreen",
        icons: [
          {
            src: path.resolve("src/assets/icon.png"),
            sizes: [96, 128, 192, 256, 384, 512]
          }
        ],
        lang: "en-US",
        name: "Chain Reaction",
        orientation: "any",
        short_name: "Chain Reaction",
        start_url: ".",
        theme_color: "#232323"
      })
    ]
  };
  if (env.prod)
    config.plugins.push(
      new OfflinePlugin({
        AppCache: false,
        ServiceWorker: {
          events: true
        }
      })
    );
  return config;
};
